import React from 'react'
import LeftSidebar from '../../components/dashboard/leftSidebar';
import TopNavbar from '../../components/dashboard/topBar';
const  Dashboard = () => {
  return (
    <div>
        <TopNavbar />
        <LeftSidebar />

    </div>
  )
}

export default Dashboard;